(ns interest-insight.test_mortgage)

(require '[clojure.data.csv :as csv]
         '[clojure.java.io :as io]
         '[semantic-csv.core :as sc]
         '[clojure.spec.alpha :as spec]
         '[clojure.spec.test.alpha :as stest]
         '[clojure.spec.gen.alpha :as gen]
         '[clojure.test :as t]
         '[clojure.algo.generic.math-functions :refer [approx=]]
         '[interest-insight.mortgage :as m]
         '[interest-insight.specs :refer :all])

;; TODO: Test function specs; maybe wait for library support to improve
(stest/instrument)

(t/deftest monthlyPaymentThaxtonTest []
  (t/is (approx= (m/monthly-payment m/thaxton) 1285.19 0.0001)))


(t/deftest paymentsRemainingThaxtonTest []
  (t/is
    (= (int (m/payments-remaining (assoc m/thaxton :payment (m/monthly-payment m/thaxton))))
       360)))


(t/deftest totalInterestPaidThaxtonTest []
  (t/is
    (approx= (m/total-interest-paid m/thaxton) 201420.56 0.01)))


(t/deftest totalInterestSavedThaxtonTest []
  (t/is
    (approx=
      (m/total-interest-saved (assoc m/thaxton :extraMonthlyPayment 400))
      83054.45 0.01)))


(t/deftest firstThaxtonPaymentTest []
  (t/is
    (approx=
      (m/new-loan-balance 4.25 1285.19 261250.00)
      260890.07 0.01)))


(def thaxton-amort-file-path "resources/thaxton_amortization.csv")

(def parseDouble #(Double/parseDouble %))
(def parseInt #(Integer/parseInt %))

;; TODO: use below as part of test
(defn thaxton-amort
  [] 
  (with-open [reader (io/reader thaxton-amort-file-path)]
    (->>
     (csv/read-csv reader)
     (sc/remove-comments)
     (sc/mappify)
     (sc/cast-with {:number parseInt :payment parseDouble
                    :interest parseDouble 
                    :principal parseDouble :balance parseDouble})
     doall)))

