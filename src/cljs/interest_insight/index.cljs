(ns ^{:hoplon/page "index.html"} interest-insight.index
  (:require
    [hoplon.core :as h]
    [hoplon.jquery :as hjq]  ;; need this or input events won't work!
    [javelin.core :as j]
    [interest-insight.mortgage :as im]
    [interest-insight.dataflow :as df]
    [interest-insight.helper :as hlp]
    [interest-insight.common-elems :as ce]
    [interest-insight.plot :as p]))

(defonce min-interest 1.0)
(defonce max-interest 25.0)

(def price-input (ce/int-cell-input :cell df/price :change-fn df/update-price!))
(def down-payment-input (ce/int-cell-input :cell df/down-payment :change-fn df/update-down-payment!))

(def percent-down-input
  (ce/float-cell-input
    :min "0.00" :max "99.99" :step "0.1"
    :cell df/percent-down :change-fn df/update-percent-down!))

(def rate-input
  (ce/float-cell-input
    :cell df/interest
    :min (str min-interest) :max (str max-interest)
    :step "0.05"))

(def term-input
  (ce/int-cell-input
    :min "6" :max "3600" :cell df/term))

(def loan-start-month-input
  (h/select (hlp/make-num-options 1 12 df/current-month)
            :change #(reset! df/loan-start-month (js/parseInt @%))))

(def loan-start-year-input
  (h/select (reverse (hlp/make-num-options 1900 df/current-year))
            :change #(reset! df/loan-start-year (js/parseInt @%))))

;; Builder functions for elements
(defn term-button
  [t]
  (h/button :click #(reset! df/term (* 12 t))
            (str t "-year term")))

(defn interest-button
  [id amount]
  (h/button :id id
            :click #(reset! df/interest
                            (hlp/bound-in (+ amount @df/interest) min-interest max-interest))
            ; below: label of button
            (if (pos? amount) (str "+" amount "%") (str amount "%"))))
;; End builder functions for elements

;; Div elements
(def header
  [(h/h1 :id "header"
        "Interest Insight")
   (h/hr :style "margin: 0px 0px 20px 0px;")])


(def loan-info-section
  (h/fieldset :id "loan-info-section" :style "border: 1px black solid"
              (hlp/fieldset-legend :title "Loan Info")
              (h/label :id "priceLabel" "The purchase price was  ") price-input
              (h/br)
              (h/label "I paid  ") percent-down-input (h/label "  percent down or  ") down-payment-input
              (h/br)
              (h/label :id "principalLabel" "So the loan had a starting principal of  ") df/principal
              (h/br) (h/br)
              (h/label "Annual interest rate (APR) is  ") rate-input (h/label " %")
              (h/br)
              (interest-button "plusQuarterButton" 0.25)
              (interest-button "plusHalfButton" 0.5)
              (interest-button "plusOneButton" 1.0)
              (h/br)
              (interest-button "minusQuarterButon" -0.25)
              (interest-button "minusHalfButton" -0.5)
              (interest-button "minusOneButton" -1.0)
              (h/br) (h/br) (h/br)
              (h/label "The length (term) of the loan is  ") term-input (h/label "  months")
              (h/br)
              (term-button 15) (term-button 20) (term-button 30)
              (h/br) (h/br)
              (h/label "The first loan payment was   ") loan-start-month-input (h/label "  /  ") loan-start-year-input
              (h/br) (h/br)
              (h/label "Paying no extra, the loan is paid off on ")
              (h/b df/loan-end-date-month) (h/b "/") (h/b df/loan-end-date-year)))



(def payment-section
  (h/fieldset :id "payment-section" :style "border: 1px black solid"
              (hlp/fieldset-legend :title "Payment")
              (h/label "Required principal/interest: ") (h/b df/monthly-required-payment)
              (h/br) (h/br)
              (h/label "Insurance:  ")
              (ce/int-cell-input :cell df/insurance)
              (h/br) (h/br)
              (h/label "Taxes:  ")
              (ce/int-cell-input :cell df/taxes)
              (h/br) (h/br)
              (h/label "Extra principal payment:  ")
              (ce/int-cell-input :step "5" :cell df/extra-monthly-payment)
              (h/br) (h/br)
              (h/label "Total payment: ") (h/b df/total-monthly-payment)
              (h/br) (h/br)
              (h/label "The loan will be paid off " df/term-reduction " months early  "
                       (h/text "(~{df/term-reduction-years} years), around ")
                       (h/label df/loan-payoff-goal-month (h/label "  /  ") df/loan-payoff-goal-year))))


(def controls
  (h/div :id "controls"
         loan-info-section
         payment-section))


(def loan-balances-plot-canvas
  (h/canvas :id "loan-balances-plot-canvas"))


(def loan-balances-plot-section
  (h/div :id "loan-balances-plot-section"
         loan-balances-plot-canvas))


(def plot-context (.getContext loan-balances-plot-canvas "2d"))
;; End of div elements  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; plotting  -----------------------------------------------------------------------------
(def balance-line-datasets
  (j/cell=
    [{:label "Balance"
      :data  (im/loan-balances {:interest df/interest
                                :balance  df/principal
                                :payment  df/total-monthly-principal-payment})
      :pointRadius 5}]))


(def balance-line-labels
  (j/cell=
    (range df/term)))


(def balance-line-data
  (j/cell=
    {:labels   balance-line-labels
     :datasets balance-line-datasets}))


(defn tooltip-label
  [tooltip-item _]
  (let [num-months (js/parseInt (.-xLabel (nth tooltip-item 0)))
        [month year] (hlp/add-months
                       [@df/loan-start-month @df/loan-start-year] num-months)]
    (str month "/" year " (month " num-months ")")))


(defn axis-config
  [label]
  [{:scaleLabel {:display true
                 :labelString label
                 :fontSize 20}}])


(def chart
  (js/Chart.
    plot-context
    (clj->js {:type "line" :data balance-line-data
              :options {:title {:display true :text "Loan Balance"}
                        :scales {:xAxes (axis-config "Month")
                                 :yAxes (axis-config "Balance")}
                        :tooltips
                               {:titleFontSize 25
                                :bodyFontSize 25
                                :callbacks {:title tooltip-label}}}})))



(defn set-chart-data [chart data]
  (do
    (set! (.-data chart) (clj->js data))
    (.update chart 0)))  ;; passing 0 to update prevents animation (immediate render)


(def plot
  (j/cell= (set-chart-data chart balance-line-data)))

;; Evaluate the HTML
(h/html
  (h/head
    (h/link :href "app.css" :rel "stylesheet" :type "text/css"))
  (h/body
    header
    loan-balances-plot-section
    controls))