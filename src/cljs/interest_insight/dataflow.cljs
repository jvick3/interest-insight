(ns interest-insight.dataflow
  (:require
    [javelin.core :as j]
    [interest-insight.mortgage :as im]
    [interest-insight.helper :as hlp]))

(defonce now (js/Date.))
(defonce current-year (.getFullYear now))
(defonce current-month (inc (.getMonth now)))

; Cells
(def price (j/cell (im/thaxton :price)))
(def percent-down (j/cell (im/thaxton :percent-down)))
(def down-payment (j/cell (* (/ @percent-down 100.0) @price)))
(def principal (j/cell= (- price down-payment)))
(def interest (j/cell (im/thaxton :interest)))
(def term (j/cell (im/thaxton :term)))
(def extra-monthly-payment (j/cell 0))
(def insurance (j/cell 58))
(def taxes (j/cell 325))
(def monthly-required-payment
  (j/cell= (im/monthly-payment {:interest interest
                                :term     term
                                ; passing principal is intentional (original balance)
                                :balance  principal})))
(def total-monthly-principal-payment
  (j/cell= (im/toTwoDecimalPlaces (+ monthly-required-payment extra-monthly-payment))))

(def total-monthly-payment
  (j/cell= (im/toTwoDecimalPlaces
             (+ monthly-required-payment insurance taxes extra-monthly-payment))))

(def term-reduction
  ; TODO: do something about this floor...fix payments-remaining?
  (j/cell= (- term (js/Math.floor (im/payments-remaining
                                    {:interest interest
                                     :balance principal
                                     :payment total-monthly-principal-payment})))))

; May say unused but it's used in a string field (interpolated)
(def term-reduction-years
  (j/cell= (im/toDecimalPlaces 1 (/ term-reduction 12.0))))

(def loan-start-month (j/cell current-month))
(def loan-start-year (j/cell current-year))
(def loan-end-date
  (j/cell= (hlp/add-months
             [loan-start-month loan-start-year] term)))
(def loan-end-date-month
  (j/cell= (loan-end-date 0)))
(def loan-end-date-year
  (j/cell= (loan-end-date 1)))

(def loan-payoff-goal-date
  (j/cell= (let [months-remaining (- term term-reduction)]
             (hlp/add-months [loan-start-month loan-start-year] months-remaining))))
(def loan-payoff-goal-month
  (j/cell= (loan-payoff-goal-date 0)))
(def loan-payoff-goal-year
  (j/cell= (loan-payoff-goal-date 1)))
;-------------------------------------------------------------------------------------------

; Special callback functions for cells

(defn when-updated [cell fn]
  ; Note: fourth argument (%4) to callback for add-watch is the new value.
  (add-watch cell nil #(fn %4)))

; Control updates to price, percent-down, and down-payment manually
; because they have a complex relationship.
; Trying to do this in Javelin formulas or callbacks led to infinite event loops,

(defn update-percent-down! [val]
  (j/dosync
    (reset! percent-down val)
    (reset! down-payment (im/toTwoDecimalPlaces (* (/ val 100.0) @price)))))


(defn update-down-payment! [val]
  (j/dosync
    (reset! down-payment val)
    (reset! percent-down (im/toTwoDecimalPlaces (* 100.0 (/ val @price))))))


(defn update-price! [val]
  (j/dosync
    (reset! price val)
    (update-down-payment! (im/toTwoDecimalPlaces (* (/ @percent-down 100.0) val)))))

; When the principal changes, reset the balance to so it isn't greater than the principal.
; (when-updated principal #(reset! balance (min @balance %)))