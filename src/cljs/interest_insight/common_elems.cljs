(ns interest-insight.common-elems
  (:require
    [hoplon.core :as h :refer [defelem]]
    [hoplon.jquery :as hjq]))  ;; need this or input events won't work!


(defelem float-cell-input
         [ {:keys [cell change-fn]
            :or {change-fn (partial reset! cell)}
            :as attributes} _]
         (h/input :type "number" :min "0.0" :max "1000000000.0" :step 0.1
                  :value cell
                  ; Default change function, can be overridden by keyword arg.
                  :change #(change-fn (js/parseFloat @%))
                  attributes))


(defelem int-cell-input
         [ {:keys [cell change-fn]
            :or {change-fn (partial reset! cell)}
            :as attributes} _]
         (h/input :type "number" :min "0" :max "1000000000" :step 1
                  :value cell
                  ; Change function; note this can still be overridden
                  ; the caller by specifying the :change keyword arg.
                  :change #(change-fn (js/parseInt @%))
                  attributes))


