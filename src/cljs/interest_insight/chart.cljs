(ns interest-insight.chart
  (:require
    [clojure.string :as str]
    [hoplon.svg :as svg]
    [hoplon.core :refer-macros [defelem]]
    [javelin.core :as j]))

;; Taken from: https://github.com/hoplon/demos/blob/master/plotSVG/src/chart.cljs.hl

(defrecord Chart [width height min-x max-x min-y max-y])


(defn config [& {:keys [width height min-x max-x min-y max-y]}]
  (Chart. width height min-x max-x min-y max-y))


(defn rel-coord
  [{:keys [width height min-x max-x min-y max-y]} x y]
  (let [w  (- max-x min-x)
        h  (- max-y min-y)
        dw (- x min-x)
        dh (- y min-y)]
    [(* width (/ dw w)) (* height (- 1 (/ dh h)))]))


(defelem container
           [{:keys [chart] :as attr} kids]
         (j/cell-let [{:keys [width height]} chart]
                   (svg/svg (assoc (dissoc attr :chart) :width width :height height) kids)))


(defelem polyline [{:keys [chart data] :as attr} _]
         (let [rels   (j/cell= (for [[x y] data]
                                (let [[x' y'] (rel-coord chart x y)]
                                  (str x' "," y'))))
               points (j/cell= (str/join " " rels))]
           ((svg/polyline :points points) (dissoc attr :chart :data))))