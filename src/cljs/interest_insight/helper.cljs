(ns interest-insight.helper
  (:require [hoplon.core :as h]))


(defn bound-in
  [val lower upper]
  (max (min val upper) lower))


(defn fieldset-legend
  [& {:keys [title font-size]
      :or {font-size 20}}]
  (h/legend title
            :style (str "border: 1px black solid; font: " font-size "px arial;")
            :align "center"))


(defn add-months
  [[month year] amount]
  ; The reason for the dec and inc on month
  ; is because Javascript Date's have zero-indexed
  ; months but the month input to this function is one indexed
  ; (i.e. March is 3 but in Javascript it's 2)
  (let [d (js/Date. year (dec month) 1)]
    (.setMonth d (dec (+ amount month))) ; need to dec because of zero-indexing again
    [(inc (.getMonth d)) (.getFullYear d)]))


(defn make-num-option
  [n]
  (h/option :value (str n) (str n)))

(defn make-num-options
  ([start end]
   (map make-num-option (range start (inc end))))
  ([start end selected]
   (flatten [(make-num-options start (dec selected))
             (h/option :value (str selected) :selected true (str selected))
             (make-num-options (inc selected) end)])))