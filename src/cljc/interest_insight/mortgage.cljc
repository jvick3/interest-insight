(ns interest-insight.mortgage
  (:require #?(:clj [clojure.spec.test.alpha :as stest] :cljs [cljs.spec.test.alpha :as stest])
            #?(:clj [clojure.spec.alpha :as s] :cljs [cljs.spec.alpha :as s])
            [interest-insight.specs :as mspec]))


(defn monthly-interest
  "Computes fractional interest per-month from annual rate given as percentage"
  [interest]
  (double (/ interest 100 12)))

;; Reader conditionals; define functions that use Java/Javascript math libraries.
(def round #?(:clj #(Math/round %)
              :cljs #(js/Math.round %)))

(def logarithm #?(:clj #(Math/log %)
                  :cljs #(js/Math.log %)))

(def power #?(:clj #(Math/pow %1 %2)
              :cljs #(js/Math.pow %1 %2)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defn toDecimalPlaces
  "Rounds a decimal number to the specified number of places"
  [numPlaces val]
  (let [factor (power 10 numPlaces)]
    (double (/ (round (* factor val)) factor))))


(def toTwoDecimalPlaces (partial toDecimalPlaces 2))


(defn monthly-payment 
  "Calculates the monthly payment amount on a loan -
  does not include taxes, insurance, etc. - principal/interest only.
  'interest' is the yearly interest rate as a percentage (e.g. 4.25), 'term' is
  the length of the loan in months (e.g. 360 for 30 years), 
  and 'balance' is the amount (principal) of the loan."
  [{:keys [interest term balance]}]
  (let [monthlyRate (monthly-interest interest)
        onePlusRToN (power (inc monthlyRate) term)
        numerator (* monthlyRate balance onePlusRToN)
        denominator (dec onePlusRToN)]
    (toTwoDecimalPlaces (/ numerator denominator))))


;; TODO: this function is returning a decimal instead of an integer.
;;       You can't make a partial payment.
(defn payments-remaining
  "Computes number of payments required to pay off the loan, 
  given the current loan amount, APR interest rate as percentage,
  and monthly interest.  Monthly interest can be computed by
  the monthly-interest function."
  ([{:keys [interest balance payment]}]
   (let [monthlyR (monthly-interest interest)
         top (* -1 (logarithm (- 1 (/ (* monthlyR balance) payment))))
         bottom (logarithm (inc monthlyR))]
    (/ top bottom))))


(defn total-interest-paid
  "Computes total amount paid into loan over term.  rate is 
  annual interest rate as percentage; P is principal; N is 
  number of payments."
  ([{:keys [balance extraMonthlyPayment]
         :or {extraMonthlyPayment 0}  ;; default to no extra monthly interest (zero)
         :as data}]
   (let [requiredMonthlyPayment (monthly-payment data)
         actualMonthlyPayment (+ requiredMonthlyPayment extraMonthlyPayment)
         paymentsLeft (payments-remaining (assoc data :payment actualMonthlyPayment))
         totalAmountPaid (* actualMonthlyPayment paymentsLeft)]
     (toTwoDecimalPlaces (- totalAmountPaid balance)))))


(defn interest-due
  "Computes amount of interest due at a point in time of a loan.
   rate is annual interest percentage (e.g. 4.25), P is current principal
   at time interest is applied."
  [r P]
  (* (monthly-interest r) P))


(defn new-loan-balance
  "Given the interest rate, payment being made, and current principal balance
   of a loan, return the balance after making the payment.  This accounts
   for the interest due at the time of payment and deducts the balance
   of the loan by (- payment interest)."
   [interest payment balance]
   (let [ interest (interest-due interest balance)
          paidToPrinciple (- payment interest)]
    (if (<= balance payment ) 0.00  ;; TODO: there's an edge case.  Last payment goes up slightly to cover interest.
        (toTwoDecimalPlaces (- balance paidToPrinciple)))))


(defn loan-balances
  "Computes the loan balance amortization over time until it is paid off.
   interest is annual interest rate as percentage (e.g. 4.25),
   balance is remaining amount of loan.  payment is the amount paid to the loan
   each month, which may exceed the amount to pay it off in the regular term.
   months-left (optional) specifies how long the loan has left."
  ([{:keys [interest balance payment]}]
   (let [f (partial new-loan-balance interest payment)]
     (vec (for [b (iterate f balance) :while (> b 0.00)] b)))))


(defn total-interest-saved
  "Computes total amount of interest saved (not paid by finishing loan early)
   by applying an extra payment to principal each month."
  [loanData]
  (let [loanDataWithNoExtra (assoc loanData :extraMonthlyPayment 0)
        expectedInterestPaid (total-interest-paid loanDataWithNoExtra)
        actualInterestPaid (total-interest-paid loanData)]
    (toTwoDecimalPlaces (- expectedInterestPaid actualInterestPaid))))


(def thaxton {:interest 4.25 :term 360 :balance 261250.00 :price 275000 :percent-down 5 :down-payment 13750})


;; Helper functions for function specs
(defn every-less-than
  [x coll]
  (every? #(< % x) coll))
;; End of helper functions ---------------------------------------------

;; function specs
(s/fdef monthly-interest
        :args (s/cat :interest ::mspec/interest)
        :ret (s/and number? #(>= % 0))
        :fn #(<= (% :ret)  (-> % :args :interest)))


(s/fdef toDecimalPlaces
        :args (s/cat :numPlaces pos-int?
                     :val number?)
        :ret number?)


(s/fdef monthly-payment
        :args ::mspec/basic-mortgage
        :ret (s/and number? pos?))


(s/fdef payments-remaining
        :args ::mspec/payment-info
        :ret number?)  ;; TODO: this :ret spec should really be an int.


(s/fdef total-interest-paid
        :args ::mspec/basic-mortage-with-extra-payment
        :ret (s/and number? pos?))


(s/fdef interest-due
        :args (s/cat :r ::mspec/interest :P ::mspec/balance)
        :ret (s/and number? pos?))


(s/fdef new-loan-balance
        :args (s/cat :interest ::mspec/interest :payment ::mspec/payment :balance ::mspec/balance)
        :ret (s/and number? pos?))


(s/fdef total-interest-saved
        :args ::mspec/basic-mortage-with-extra-payment
        ;; If extra monthly payment is zero, interest saved should be zero.  Otherwise it should be positive.
        :ret #(if (= (-> % :args :extraMonthlyPayment) 0)
                0
                (s/double-in :min 0.00000001 :infinite? false :NaN? false)))


(s/fdef loan-balances
        :args ::mspec/payment-info
        :ret (s/and ::mspec/decreasingNumbers         ;; test all numbers are decreasing (i.e. you're paying down the loan)
                    #(every-less-than % :balance)))   ;; test that all balances less than starting balance
;; End of function specs

;; TODO: Use specs for test suite.

;; To turn on spec instrumentation (checking), uncomment line below.
;; Must have specs namespace loaded into repl to register specs.
;; Try (monthly-payment -1) to test that specs are loaded correctly.
;;(stest/instrument)

;; Note that :fn specs are only used as part of tests, e.g. (stest/check `monthly-interest)