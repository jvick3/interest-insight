(ns interest-insight.specs
  (:require #?(:clj [clojure.spec.alpha :as s]
               :cljs [cljs.spec.alpha :as s])))


;; Data specs ---------------------------------------
(s/def ::interest (s/double-in :min 0.01 :max 100 :infinite? false :NaN? false))

(s/def ::balance (s/double-in :min 100 :infinite? false :NaN? false))

(s/def ::term (s/int-in 1 1200));; 1200 months == 100 years

(s/def ::payment (s/double-in :min 50.0 :infinite? false :NaN? false))

(s/def ::extraMonthlyPayment (s/double-in :min 0 :infinite? false :NaN? false))

;; The 'apply' applies > to the numbers in collection,
;; e.g. (> x1 x2 x3 x4 ...).  Without apply, this is just (> coll)
;; and both > and < always return true with one argument.
(s/def ::decreasingNumbers (s/and (s/coll-of number? :distinct true)
                                  #(not (empty? %))
                                  #(apply > %)))

(s/def ::basic-mortgage (s/cat :argsMap (s/keys :req-un [::interest ::term ::balance])))

(s/def ::basic-mortage-with-extra-payment (s/and ::basic-mortgage
                                                 (s/keys :opt-un [::extraMonthlyPayment])))

(s/def ::payment-info (s/cat :argsMap (s/keys :req-un [::interest ::balance ::payment])))
;; End of specs --------------------------------