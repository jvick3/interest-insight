(defproject
  boot-project
  "0.0.0-SNAPSHOT"
  :dependencies
  [[adzerk/boot-cljs "2.1.5"]
   [adzerk/boot-reload "0.6.0"]
   [adzerk/boot-test "1.2.0" :scope "test"]
   [hoplon "7.2.0"]
   [cljsjs/chartjs "2.6.0-0"]
   [org.clojure/clojure "1.9.0"]
   [org.clojure/test.check "0.10.0-alpha2"]
   [org.clojure/clojurescript "1.10.439"]
   [tailrecursion/boot-jetty "0.1.3"]
   [org.clojure/data.csv "0.1.4"]
   [semantic-csv "0.1.0"]
   [org.clojure/algo.generic "0.1.2"]
   [boot/core "2.6.0" :scope "compile"]]
  :repositories
  [["clojars" {:url "https://repo.clojars.org/"}]
   ["maven-central" {:url "https://repo1.maven.org/maven2"}]]
  :source-paths
  ["src/cljs" "src/cljc" "test"])