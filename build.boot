(set-env!
  :dependencies '[[adzerk/boot-cljs "2.1.5"]
                  [adzerk/boot-reload "0.6.0"]
                  [adzerk/boot-test "1.2.0" :scope "test"]
                  [hoplon/hoplon "7.2.0"]
                  [cljsjs/chartjs "2.6.0-0"]
                  [org.clojure/clojure "1.9.0"]
                  [org.clojure/test.check "0.10.0-alpha2"]
                  [org.clojure/clojurescript "1.10.439"]
                  [tailrecursion/boot-jetty "0.1.3"]
                  [org.clojure/data.csv "0.1.4"]
                  [semantic-csv "0.1.0"]
                  [org.clojure/algo.generic "0.1.2"]]
  :source-paths #{"src/cljs", "src/cljc", "test"}
  :asset-paths  #{"assets"})


(require
  '[adzerk.boot-cljs         :refer [cljs]]
  '[adzerk.boot-test :refer :all]
  '[adzerk.boot-reload       :refer [reload]]
  '[hoplon.boot-hoplon       :refer [hoplon prerender]]
  '[tailrecursion.boot-jetty :refer [serve]])

(deftask dev
  "Build interest-insight for local development."
  []
  (comp
    (test)
    (watch)
    (speak)
    (hoplon)
    (reload)
    (cljs :optimizations :simple)
    (serve :port 8000 :init-params {"org.eclipse.jetty.servlet.Default.useFileMappedBuffer" "false"})))

(deftask prod
  "Build interest-insight for production deployment."
  []
  (comp
    (test)
    (hoplon)
    (cljs :optimizations :simple)
    (target :dir #{"target"})))
